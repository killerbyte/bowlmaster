﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public Ball ball;
    private bool dragStart;
    private bool trackingHead = true;
    public CardboardHead head;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if (ball.inPlay)
        {
            DisableHeadTracking();
        }
        else
        {
            EnableHeadTracking();
        }

	    if (Cardboard.SDK.Triggered)
        {
            if (!ball.inPlay)
            {
                if (!dragStart)
                {
                    dragStart = true;
                    ball.GetComponent<DragLaunch>().DragStart();
                }
                else
                {
                    ball.GetComponent<DragLaunch>().DragEnd();
                    dragStart = false;
                }
            }
        }
	}

    void DisableHeadTracking()
    {
        if (trackingHead)
        {
            head.trackPosition = false;
            head.trackRotation = false;
            trackingHead = false;
        }
    }

    void EnableHeadTracking()
    {
        if (!trackingHead)
        {
            head.trackPosition = true;
            head.trackRotation = true;
            trackingHead = true;
        }
    }
}
