﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinSetter : MonoBehaviour
{
    public GameObject pinSet;

    private Animator animator;
    private PinCounter pinCounter;

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        pinCounter = GameObject.FindObjectOfType<PinCounter>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void RaisePins()
    {
        // Raise standing pins by distanceToRaise
        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            pin.Raise();
        }
    }

    public void LowerPins()
    {
        // Lower pins back down
        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            pin.Lower();
        }
    }

    public void RenewPins()
    {
        DestroyPins();
        GameObject newPins = Instantiate(pinSet);
        newPins.transform.position += new Vector3(0, 50, 0);
        //Instantiate(pinSet, new Vector3(0, 40, 1829), Quaternion.identity);
    }

    // Make sure the game object is destroyed
    void DestroyPins()
    {
        if (GameObject.Find("Pins"))
        {
            Destroy(GameObject.Find("Pins"));
        }
        else
        {
            Destroy(GameObject.Find("Pins(Clone)"));
        }
    }

    public void PerformAction(ActionMaster.Action action)
    {
        if (action == ActionMaster.Action.Tidy)
        {
            animator.SetTrigger("tidyTrigger");
        }
        else if (action == ActionMaster.Action.Reset || action == ActionMaster.Action.EndTurn)
        {
            animator.SetTrigger("resetTrigger");
            pinCounter.Reset();            
        }
        else if (action == ActionMaster.Action.EndGame)
        {
            throw new UnityException("Don't know how to handle just yet!");
        }
    }
}
