﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    public Vector3 launchSpeed;
    public bool inPlay = false;

    private Rigidbody rb;
    private AudioSource ballSound;
    private Vector3 ballStartPos;

	// Use this for initialization
	void Start ()
    {
        // Retrieve components first
        rb = GetComponent<Rigidbody>();
        ballSound = GetComponent<AudioSource>();
        rb.useGravity = false;
        ballStartPos = transform.position;
	}

    public void Launch(Vector3 velocity)
    {
        inPlay = true;
        rb.useGravity = true;
        rb.velocity = velocity;
        ballSound.Play();
    }

    public void Reset()
    {
        inPlay = false;
        transform.position = ballStartPos;
        transform.rotation = Quaternion.identity;


        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = false;
    }
}
