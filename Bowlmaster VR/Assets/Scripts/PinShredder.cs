﻿using UnityEngine;
using System.Collections;

public class PinShredder : MonoBehaviour
{
    void OnTriggerExit(Collider collider)
    {
        GameObject thingLeft = collider.gameObject;

        // Pin left the play box
        if (thingLeft.GetComponent<Pin>())
        {
            Destroy(thingLeft);
        }
    }
}
