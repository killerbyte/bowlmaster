﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinCounter : MonoBehaviour
{
    public Text standingPinDisplay;

    private bool ballOutOfPlay = false;
    private int lastStandingCount = -1;
    private float lastChangeTime;
    private int lastSettledCount = 10;
    private const int MAX_NUM_PINS = 10;

    private GameManager gameManager;

    // Use this for initialization
    void Start () {
        gameManager = GameObject.FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        standingPinDisplay.text = CountStandingPins().ToString();

        if (ballOutOfPlay)
        {
            UpdateStandingCountAndSettle();
            standingPinDisplay.color = Color.red;
        }
    }

    public void Reset()
    {
        lastSettledCount = MAX_NUM_PINS;
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name == "BowlingBall")
        {
            ballOutOfPlay = true;
        }
    }

    void UpdateStandingCountAndSettle()
    {
        // Update the lastStandingCount
        // Call PinsHaveSettled when they have

        int currentStanding = CountStandingPins();

        if (currentStanding != lastStandingCount)
        {
            lastChangeTime = Time.time;
            lastStandingCount = currentStanding;
            return;
        }

        float settleTime = 3f;
        if ((Time.time - lastChangeTime) > settleTime) // Check for settled pins
        {
            PinsHaveSettled();
        }
    }

    void PinsHaveSettled()
    {
        int standing = CountStandingPins();
        int pinFall = lastSettledCount - standing;
        lastSettledCount = standing;

        gameManager.Bowl(pinFall);

        lastStandingCount = -1; // Pins have settled
        standingPinDisplay.color = Color.green;
        ballOutOfPlay = false;
    }

    int CountStandingPins()
    {
        int standing = 0;

        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            if (pin.isStanding())
            {
                standing++;
            }
        }

        return standing;
    }
}
