﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TouchInputScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Ball ball;

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        ball.GetComponent<BallDragLaunch>().DragStart();
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        ball.GetComponent<BallDragLaunch>().DragEnd();
    }
}
